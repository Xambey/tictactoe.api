using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using TicTacToe.Api.DataBase;

namespace TicTacToe.Api.Hubs
{
    public class LobbyListHub : Hub
    {
        private readonly DbContext _dbContext;

        public LobbyListHub(DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task CreateRoom(string roomName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, roomName);
            await Update();
        }

        public async Task Update()
        {
            var rooms = _dbContext.GetRooms();
            await Clients.All.SendCoreAsync("update", rooms.Cast<object>().ToArray());
        }
    }
}