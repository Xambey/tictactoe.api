using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace TicTacToe.Api.Models
{
    public class Room
    {
        public List<IdentityUser> Users { get; set; }
        public string Name { get; set; }
    }
}