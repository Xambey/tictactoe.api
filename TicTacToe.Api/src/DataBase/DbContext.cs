using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using TicTacToe.Api.Models;

namespace TicTacToe.Api.DataBase
{
    public class DbContext
    {
        private static readonly object LockRooms = new object();
        private static List<Room> Rooms { get; set; } = new List<Room>();

        public void AddRoom(Room room)
        {
            lock (LockRooms)
            {
                Rooms.Add(room);
            }
        }

        public void RemoveRoom(Room room)
        {
            lock (LockRooms)
            {
                Rooms.Remove(room);
            }
        }

        public List<Room> GetRooms()
        {
            return Rooms;
        }
    }
}